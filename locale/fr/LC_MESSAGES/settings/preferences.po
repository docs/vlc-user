# SOME DESCRIPTIVE TITLE.
# Copyright (C) This page is licensed under a CC-BY-SA 4.0 Int. License
# This file is distributed under the same license as the VLC User
# Documentation package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: VLC User Documentation \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-02-06 22:33+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

# cdd637170d6d4f27a8b6f62c295dad85
#: ../../settings/preferences.rst:3
msgid "Preferences"
msgstr ""

# a44c7189a44d425fbbaf6f6925110164
#: ../../settings/preferences.rst:5
msgid ""
"This menu gives you access to all the settings that establish the "
"behaviour of VLC. Each section of preferences comes in two flavours: "
":guilabel:`Simple` and :guilabel:`Advanced`. The simple preferences are "
"settings for the most commonly used parameters of the application while "
"the advanced settings require some deeper knowledge to use them "
"effectively."
msgstr ""

# b5dbe8e7ca8f4e0e895180830f857ef8
#: ../../settings/preferences.rst:10
msgid "Where to find VLC preferences"
msgstr ""

# 04d78e03c2ac4538b0fe0f76a24dcd86
#: ../../settings/preferences.rst:12
msgid ""
"To open the Preferences panel, select :menuselection:`Tools --> "
"Preferences`."
msgstr ""

# b9a2571a50b5476f96f1e9f1f016555d
#: ../../settings/preferences.rst:17
msgid ""
"Here is the :guilabel:`Simple Preferences` panel where you can modify the"
" essential settings of VLC."
msgstr ""

# 3a918dd5d2ef470e9fe80ba7c0670496
#: ../../settings/preferences.rst:24
msgid "How to reset the VLC preferences"
msgstr ""

# 9792a26cdc92423297db72768abde774
#: ../../settings/preferences.rst:26
msgid ""
"Many problems with VLC are due to incorrect settings or a broken plugin "
"cache. You can solve these by deleting VLC's preferences and cache. To do"
" this, open VLC on computer, click on :menuselection:`Tools --> "
"Preferences --> Reset Preferences`."
msgstr ""

# bc4722ca3638430283ca1b71bc5e07ac
#: ../../settings/preferences.rst:31
msgid "Preference sections"
msgstr ""

# 4d66494e219841bfaad2e74f140c40a4
#: ../../settings/preferences.rst:33
msgid "Interface"
msgstr ""

# f9eb20299f5a41338219ffc54ceef2d1
#: ../../settings/preferences.rst:34
msgid "Choosing and configuring an alternate interface."
msgstr ""

# e4da818e030647008601ad85e87dd5c1
#: ../../settings/preferences.rst:35
msgid "Audio"
msgstr ""

# 36be13de66264a2696455f1718f5622c
#: ../../settings/preferences.rst:36
msgid "Allows you to choose the type audio to play through which channels."
msgstr ""

# b3c95dd7dff448b3a3625a4382623d3f
#: ../../settings/preferences.rst:37
msgid "Video"
msgstr ""

# b14ae7a134dc4789ba533643b33257a5
#: ../../settings/preferences.rst:38
msgid "An option to choose which video to play through which channels."
msgstr ""

# 54ceb08019d14fd3a8809a0e6353b499
#: ../../settings/preferences.rst:39
msgid "Subtitles/OSD"
msgstr ""

# aeb55fafb8064082a26687c43988bc43
#: ../../settings/preferences.rst:40
msgid "Here, you can make changes to subtitles and On Screen Display Settings."
msgstr ""

# d6e0ac682e5941f2a0b0706c6cf13e95
#: ../../settings/preferences.rst:41
msgid "Input / Codecs"
msgstr ""

# 6736ac3e0e0a4c659e298d86cc579898
#: ../../settings/preferences.rst:42
msgid "How to interpret the video file/stream."
msgstr ""

# 88a4e7cfbd394661a4d798bbf4716947
#: ../../settings/preferences.rst:43
msgid "Hotkeys"
msgstr ""

# 39419f34a9fd42aab222318333bfb66a
#: ../../settings/preferences.rst:44
msgid "Keyboard shortcuts used in VLC."
msgstr ""

